package test;

import org.junit.Test;

import org.junit.Assert;
import prograiii_tp1.objetos.Tablero;
import prograiii_tp1.utilidades.excepciones.FilaColumnaInexistenteException;

public class TableroTest {

	
	@Test
	public void testNuevoTableroYBuscarNumeroEnPosicionExistente () throws FilaColumnaInexistenteException {
		Tablero tablero = new Tablero();
		int resultado = tablero.getValor(1, 2);
		Assert.assertTrue("El resultado debe ser entre 0 y 4", resultado>=0&&resultado<=4);
	}

	@Test (expected = FilaColumnaInexistenteException.class)
	public void testNuevoTableroYBuscarNumeroEnPosicionInexistentee () throws FilaColumnaInexistenteException {
		Tablero tablero = new Tablero();
		tablero.getValor(10, 10);
	}
}
