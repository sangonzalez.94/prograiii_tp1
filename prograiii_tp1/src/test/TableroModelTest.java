package test;

import static org.junit.Assert.*;

import org.junit.Test;

import prograiii_tp1.modelos.TableroModel;
import prograiii_tp1.utilidades.ConstantesUtil;

/**
 * Test unitario de la clase TableroModel
 * @author santiago
 *
 */
public class TableroModelTest {

	@Test
	public void testIniciarTableroModelConDosNumeros() {
		TableroModel modelo = new TableroModel();
		int[][] tablero = modelo.getTablero();
		int cantNumeros = 0;
		
		for(int fila = 0; fila < ConstantesUtil.longitudFila; fila++) {
			for (int columna = 0; columna < ConstantesUtil.longitudColumna; columna++) {
				if(tablero[fila][columna]>0) {
					cantNumeros++;
				}
			}
		}
		
		assertTrue("la cantidad de números debe ser 2", cantNumeros==2);
	}
	
	@Test
	public void testIniciarTableroModelConDosNumerosYSumarleUnNumero() {
		TableroModel modelo = new TableroModel();
		int[][] tablero = modelo.getTablero();
		int cantNumeros = 0;
		
		for(int fila = 0; fila < ConstantesUtil.longitudFila; fila++) {
			for (int columna = 0; columna < ConstantesUtil.longitudColumna; columna++) {
				if(tablero[fila][columna]>0) {
					cantNumeros++;
				}
			}
		}
		
		assertTrue("la cantidad de números debe ser 2", cantNumeros==2);
		
		modelo.nuevoNumeroEnTablero();
		tablero = modelo.getTablero();
		cantNumeros = 0;
		
		for(int fila = 0; fila < ConstantesUtil.longitudFila; fila++) {
			for (int columna = 0; columna < ConstantesUtil.longitudColumna; columna++) {
				if(tablero[fila][columna]>0) {
					cantNumeros++;
				}
			}
		}
		assertTrue("la cantidad de números debe ser 3", cantNumeros==3);
		
	}

}
