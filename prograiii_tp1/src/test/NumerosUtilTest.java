package test;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import prograiii_tp1.utilidades.NumerosUtil;

/**
 * Test unitario de la clase NumerosUtil
 * @author santiago
 *
 */
public class NumerosUtilTest {
	private static int numeroA;
	private static int numeroB;
	private static int numeroC;
	private static int numeroD;
	
	@BeforeClass
	public static void setUp() {
		numeroA = 2;
		numeroB = 3;
		numeroC = 4;
		numeroD = 102041230;
	}
	
	@Test
	public void testVerificarDosNumerosIgualesTrue() {
		boolean resultado = NumerosUtil.sonIguales(numeroA, numeroA);
		Assert.assertTrue(resultado);
	}
	
	@Test
	public void testVerificarDosNumerosIgualesFalse() {
		boolean resultado = NumerosUtil.sonIguales(numeroA, numeroB);
		Assert.assertFalse(resultado);
	}

	@Test
	public void testVerificarCuatroNumerosIgualesTrue() {
		boolean resultado = NumerosUtil.sonIguales(numeroA, numeroA,numeroA,numeroA);
		Assert.assertTrue(resultado);
	}
	
	@Test
	public void testVerificarCuatroNumerosDistintosFalse() {
		boolean resultado = NumerosUtil.sonIguales(numeroA, numeroB,numeroC,numeroD);
		Assert.assertFalse(resultado);
	}
	@Test
	public void testVerificarTresNumerosIgualesUnoNoFalse() {
		boolean resultado = NumerosUtil.sonIguales(numeroA, numeroA,numeroC,numeroA);
		Assert.assertFalse(resultado);
	}
	
	@Test
	public void testObtenerNumeroParEntreDosYCuatro() {
		int resultado = NumerosUtil.numeroAlAzar(numeroA, numeroC, true);
		Assert.assertTrue((resultado == 2)||(resultado == 4));
	}
	@Test
	public void testObtenerNumeroEntreDosYCuatro() {
		int resultado = NumerosUtil.numeroAlAzar(numeroA, numeroC);
		Assert.assertTrue((resultado == 2)||(resultado == 4 ||(resultado == 3)));
	}
	
}
