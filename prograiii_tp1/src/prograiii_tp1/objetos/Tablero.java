package prograiii_tp1.objetos;

import prograiii_tp1.utilidades.excepciones.Excepcion;
import prograiii_tp1.utilidades.excepciones.FilaColumnaInexistenteException;


/**
 * Clase Tablero. Acá van a estar las utilidades del tablero y va a ser 
 * útil para pasar el tablero entre las distintas capas de la aplicación.
 * 
 * @author santiago
 *
 */
public class Tablero {
	private int[][] tablero;
	
	/**
	 * Crea un tablero por defecto de 4 columnas con 3 filas.
	 */
	public Tablero () {
		tablero = new int[4][4];
	}
	
	public Tablero (int [][] tablero) {
		this.tablero = tablero;
	}
	
	/**
	 * 
	 * @param fila
	 * @param columna
	 * @return
	 * @throws FilaColumnaInexistenteException
	 */
	public int getValor(int fila, int columna) throws FilaColumnaInexistenteException {
		try {
			return tablero[fila][columna];
		}catch(ArrayIndexOutOfBoundsException e) {
			throw new FilaColumnaInexistenteException(Excepcion.COLUMNA_FILA_INEXISTENTES.getMensaje());
		}
		
	}
}
