package prograiii_tp1;

import static prograiii_tp1.utilidades.TextosUtil.*;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.TextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import prograiii_tp1.controladores.TableroController;
import prograiii_tp1.objetos.Tablero;
import static prograiii_tp1.utilidades.ConstantesUtil.*;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;

public class Launcher2048 {
	private int longitudColumna = 4;
	private int longitudFila = 4;
	private JFrame frame;
	private JTextField [][] tablero;
	private TableroController tableroController;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
	EventQueue.invokeLater(new Runnable() {
		public void run() {
				try {
					Launcher2048 window = new Launcher2048();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Launcher2048() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 700, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle(NOMBRE_APLICACION);
		tablero = new JTextField[longitudFila][longitudColumna];
		tableroController = new TableroController();
		cargarMenu();
		dibujarTablero();
		inicializarTablero();
		anadirEventos();
		cargarLabels();
	}

	private void cargarLabels() {
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(10, 11, 46, 14);
		frame.getContentPane().add(lblNewLabel);
	}

	private void anadirEventos() {
		frame.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				switch(e.getKeyCode()) {
					case KeyEvent.VK_LEFT:
						accionIzquierda();
						break;
					case KeyEvent.VK_RIGHT:
						accionDerecha();
						break;
					case KeyEvent.VK_UP:
						accionArriba();
						break;
					case KeyEvent.VK_DOWN:
						accionAbajo();
						break;
					default:
							break;
				}
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	/**
	 * carga un menú en la pantalla con opciones.
	 */
	private void cargarMenu() {
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnJuego = new JMenu(MENU_JUEGO);
		menuBar.add(mnJuego);
		
		JMenuItem mntmNuevoJuego = new JMenuItem(MENU_JUEGO_NUEVO);
		mnJuego.add(mntmNuevoJuego);
		
		JMenuItem mntmMejoresPuntuaciones = new JMenuItem(MENU_JUEGO_MEJORES_PUNTUACIONES);
		mnJuego.add(mntmMejoresPuntuaciones);
		
		JMenu mnAyuda = new JMenu(MENU_AYUDA);
		menuBar.add(mnAyuda);
		
		JMenuItem mntmComoJugar = new JMenuItem(MENU_COMO_JUGAR);
		mnAyuda.add(mntmComoJugar);
		
		frame.getContentPane().setLayout(null);
		
		
		
	}
	
	/**
	 * Dibuja el tablero para mostrar los números en la pantalla.
	 * 
	 */
	private void dibujarTablero() {
		for (int fila = 0; fila < longitudFila; fila++) {
			for (int columna = 0; columna < longitudColumna; columna++) {
				tablero [fila][columna] = new JTextField();
			}
		}
		int posFila = 142;
		int posColumna = 250;
		
		for (int fila = 0; fila < 4; fila++) {
			for (int columna = 0; columna < 4; columna ++) {
				JTextField txtfield = tablero[fila][columna];
				txtfield.setBounds(posColumna, posFila, 50, 50);
				txtfield.setEnabled(false);
				txtfield.setFont(new Font(FUENTE_DE_TEXTO, Font.BOLD, 15));
				txtfield.setHorizontalAlignment(SwingConstants.CENTER);
				frame.getContentPane().add(txtfield);
				posColumna=posColumna+55;
			}
			posColumna = 250;
			posFila=posFila+50;
		}
		
		
	}
	
	/**
	 * Agrega un arreglo de elementos al frame.
	 * @param objects
	 */
	private void agregarElementosAlFrame(Object ...objects) {
		for (Object o:objects) {
			if (o instanceof TextField) {
				frame.getContentPane().add((TextField) o);
			}
		}
	}
	
	private void inicializarTablero() {
		transformarTableroAArregloTxt(tableroController.getTableroActual());
	}
	
	/**
	 * 
	 * @param tableroActual
	 */
	private void transformarTableroAArregloTxt(Tablero tableroActual) {
		try {
			for(int fila = 0; fila < longitudFila; fila++) {
				for (int columna = 0; columna < longitudColumna; columna++) {
					tablero[fila][columna].setText(Integer.toString(tableroActual.getValor(fila, columna)));
				}
			}
		}catch (Exception e) {
			
		}
		
	}

	private void accionDerecha() {
		System.out.println("DERECHA");
	}
	
	private void accionIzquierda() {
		System.out.println("IZQUIERDA");
	}
	
	private void accionAbajo() {
		System.out.println("ABAJO");
	}
	
	private void accionArriba() {
		System.out.println("ARRIBA");
	}

	private void actualizarPuntaje() {
		
	}
	
	private void mostrarMejoresPuntuaciones() {
		
	}
}
