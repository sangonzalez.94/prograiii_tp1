package prograiii_tp1.modelos;

import static prograiii_tp1.utilidades.NumerosUtil.*;

import static prograiii_tp1.utilidades.ConstantesUtil.*;

/**
 * En esta clase esta la lógica del juego.
 * @author Santiago Ezequiel Gonzalez
 *
 */
public class TableroModel {

	private int tablero[][];
	private boolean tableroCompleto;
	
	public TableroModel() {
		tablero = new int [4][4];
		tableroCompleto = false;
		this.iniciarTablero();
	}
	
	public void iniciarTablero() {
		// Pongo en cero todos los casilleros.
		for(int fila = 0; fila < longitudFila; fila++) {
			for(int columna = 0; columna < longitudColumna; columna ++) {
				tablero[fila][columna] = 0;
			}
		}
		//Agrego dos numeros al azar en el tablero en posiciones aleatorias.
		nuevoNumeroEnTablero();
		nuevoNumeroEnTablero();
	}
	
	
	/**
	 * Devuelve un objeto clon del tablero para
	 * que el tablero original no sea modificado sin 
	 * respetar las reglas del juego.
	 * @return
	 */
	public int[][] getTablero(){
		return tablero.clone();
	}
	
	public void nuevoNumeroEnTablero() {
		//Busco una posición al azar para poner un nuevo número.
		int fila = numeroAlAzar(0, longitudFila-1);
		int columna = numeroAlAzar(0, longitudColumna-1);
		int nroNuevo = numeroAlAzar(2, 4, true);
		if(tablero [fila] [columna] != 0) {
			nuevoNumeroEnTablero();
		}
		//ubico el número en la grilla generada.
		tablero[fila][columna] = nroNuevo;
		actualizarEstadoTablero();
	}

	private void actualizarEstadoTablero() {
		tableroCompleto = false;
		boolean contieneCeros = true;
		for(int fila = 0; fila < longitudFila && !contieneCeros; fila ++) {
			for (int columna = 0; columna < longitudColumna && !contieneCeros; columna ++) {
				contieneCeros = tablero[fila][columna]==0?true:false;
			}
		}
		if(!contieneCeros) {
			tableroCompleto = true;
		}
	}
	
	
}
