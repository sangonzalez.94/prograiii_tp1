package prograiii_tp1.controladores;

import prograiii_tp1.modelos.TableroModel;
import prograiii_tp1.objetos.Tablero;

/**
 * En esta clase se interactua entre la vista y el modelo de la aplicación.
 * 
 * @author Santiago Ezequiel Gonzalez
 *
 */
public class TableroController {

	private TableroModel tableroModel;
	private Tablero tablero;
	
	/**
	 * Constructor del controlador del juego.
	 */
	public TableroController() {
		tableroModel = new TableroModel();
		tablero = new Tablero(tableroModel.getTablero());
	}
	
	public Tablero getTableroActual() {
		return tablero;
	}

}
